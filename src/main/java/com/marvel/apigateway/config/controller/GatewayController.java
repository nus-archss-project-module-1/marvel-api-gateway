package com.marvel.apigateway.config.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Pradeep Kumar
 * @create 10/3/2021 11:44 PM
 */
@RestController
public class GatewayController {

    @GetMapping("/defaultFallbackPage")
    public String defaultMessage()
    {
        return "Server is busy, Please try again later...";
    }
}
