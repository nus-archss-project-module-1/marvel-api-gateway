package com.marvel.apigateway.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.ReactiveResilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.gateway.filter.factory.DedupeResponseHeaderGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.PredicateSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.function.Function;

@Configuration
public class MarvelSpringCloudConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarvelSpringCloudConfig.class);


    @Bean
    public CorsWebFilter corsFilter() {
        return new CorsWebFilter(corsConfigurationSource());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
        config.addAllowedMethod("*");
        config.addAllowedHeader("*");
        config.addAllowedOrigin("*");
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Bean
    public RedisRateLimiter redisRateLimiter() {
        return new RedisRateLimiter(50, 100);
    }

    @Bean
    KeyResolver userKeyResolver() {
        return exchange -> Mono.just("1");
    }

    @Bean
    public Customizer<ReactiveResilience4JCircuitBreakerFactory> defaultCustomizer() {
        return factory -> factory.configureDefault(id -> new Resilience4JConfigBuilder(id)
                .circuitBreakerConfig(CircuitBreakerConfig.ofDefaults())
                .timeLimiterConfig(TimeLimiterConfig.custom()
                        .timeoutDuration(Duration.ofSeconds(30)).build()).build());
    }

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        LOGGER.info("Loading Marvel Gateway Routes....");
        RouteLocator routeLocator = builder.routes()
                .route(buildRouteFor("/marvel-student/**", "marvel-student-request", "marvel-student-header",
                        "marvel-student-response", "marvel-student-response-header", "https://student.marvelhub.org/", "marvel-student"))

                .route(buildRouteFor("/marvel-course-provider/**", "marvel-course-provider-request", "marvel-course-provider-header",
                        "marvel-course-provider-response", "marvel-course-provider-response-header", "https://courseprovider.marvelhub.org/", "marvel-course-provider"))
                .build();

        LOGGER.info("Route Locator Configs: [{}]", routeLocator.getRoutes());
        return routeLocator;
    }

    private Function<PredicateSpec, Route.AsyncBuilder> buildRouteFor(String path, String headerName, String headerValue, String responseName, String responseValue, String microServiceUri, String serviceId) {
        return r -> r.path(path)
                .filters(f -> f.addRequestHeader(headerName, headerValue)
                        .addResponseHeader(responseName, responseValue)
                        .dedupeResponseHeader("Access-Control-Allow-Credentials Access-Control-Allow-Origin", DedupeResponseHeaderGatewayFilterFactory.Strategy.RETAIN_FIRST.name())
                        .circuitBreaker(cb -> cb.setName("default-circuit-breaker").setFallbackUri("/defaultFallbackPage"))
                )
                .uri(microServiceUri).id(serviceId);
    }


}