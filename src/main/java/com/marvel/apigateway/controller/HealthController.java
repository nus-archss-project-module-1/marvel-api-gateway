package com.marvel.apigateway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("marvel-api-gateway")
public class HealthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HealthController.class);

    @RequestMapping(value = "/health", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    //@GetMapping("/health")
    public String index() {
        LOGGER.info("API Gateway is UP!");
        return "API Gateway is UP!";
    }
}
