package com.marvel.apigateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;

@SpringBootApplication(exclude = {ThymeleafAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class MarvelApiGatewayRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(MarvelApiGatewayRunner.class);

	public static void main(String[] args) {
		LOGGER.info("Starting up MarvelApiGatewayRunner...");
		SpringApplication.run(MarvelApiGatewayRunner.class, args);
	}

}
